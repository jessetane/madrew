var wsapi = require('wsapi');
var router = require('uri-router');
var PageView = require('./src/pages');
var NavView = require('./src/nav');

var api = wsapi();
var initialized = false;

api.on('connect', function() {
  if (initialized) return;
  initialized = true;

  api.remote.read('settings', function(err, data) {
    window.settings = data && data.files;

    var navrouter = router({
      watch: 'pathname',
      outlet: '.nav',
      routes: {
        '(.*)?': NavView,
      }
    });

    var pagerouter = router({
      watch: 'pathname',
      outlet: '.pages',
      routes: {
        '^/[^/]*(/.*)?': PageView,
      }
    });
  });
});

require('fastclick')(document.body);
