var fsdb = require('fsdb');

var db = fsdb({ root: __dirname + '/var' });
var date = {
  read: function(fs, fullpath, filename, address, opts, cb) {
    var self = this;
    fs.readFile(fullpath, 'utf8', function(err, data) {
      if (err) return cb(err);
      data = data.trim();
      self.files[filename].data = +new Date(data);
      cb();
    });
  },
  update: db.types.txt.update
};

module.exports = function() {
  
  var db = fsdb({
    root: __dirname + '/var', 
    types: {
      'md': 'txt',
      'html': 'txt',
      'date': date,
    },
  });

  var api = db.api();

  return {
    read: api.read,
    search: api.search,
  }
};
