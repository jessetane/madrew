var http = require('http');
var wsapi = require('wsapi');
var ecstatic = require('ecstatic');
var xtend = require('xtend/mutable');

process.env = xtend(require('./etc/env'), process.env);

var statics = ecstatic(__dirname + '/share', { cache: 'no-cache' });

var server = http.createServer(function(req, res) {
  if (!/\.[^\/]*$/.test(req.url)) {
    req.url = '/';
  }
  console.log(req.url);
  statics(req, res);
});

var api = wsapi({
  server: server,
  api: require('./api'),
});

server.listen(process.env.port, process.env.host, function() {
  console.log('server listening on ' + process.env.port);
});
