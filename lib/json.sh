# super ghetto json / js parsing

util_get_json() {
  IFS=$'\n'
  local args=("$@")
  local args="$(printf "%s|" "${args[@]}")"
  local object=()
  local line
  while read line; do
    local i=0
    for arg in "$@"; do
      local param="$(echo "$line" | grep "$arg")"
      if [ -n "$param" ]; then
        if [ -n "${object[$i]}" ]; then
          util_print_json_object $#
          object=()
        fi
        object[$i]="$(echo "$param" | sed 's|.*:[ ]*'"'"'\(.*\)'"'"'.*|\1|')"
        if ((${#object[@]} == $#)); then
          util_print_json_object $#
          object=()
        fi
        break
      fi
      ((i++))
    done
  done < <(grep -E '('"${args%?}"')')
  if [ ${#object[@]} -gt 0 ]; then
    util_print_json_object $#
  fi
  unset IFS
}

util_print_json_object() {
  local i
  local max=$(($1 - 1))
  for ((i=0; i<$1; i++)); do
    echo -n "${object[$i]}"
    if ((i < max)); then
      echo -ne "\t"
    fi
  done
  echo ""
}
