var xtend = require('xtend/mutable');
var wsapi = require('wsapi');
var api = wsapi();

module.exports = function(pathname) {

  Model.read = function(opts, cb) {
    if (typeof opts === 'function') {
      cb = opts;
      opts = null;
    }

    api.remote.read(pathname, opts, function(err, db) {
      if (err) return cb(err);
      if (typeof db === 'string') db = JSON.parse(db);
      var models = [];
      for (var i in db.folders) {
        models.push(mkmodel(db.folders[i]));
      }
      var sort = opts.sort || opts.reverse;
      if (sort) {
        models.sort(function(a, b) {
          if (opts.reverse) return a[sort] < b[sort] ? 1 : -1;
          else return a[sort] > b[sort] ? 1 : -1;
        });
      }
      cb(null, models);
    });
  };

  function Model(props) {
    xtend(this, props);
  }

  Model.prototype.read = function(opts, cb) {
    if (typeof opts === 'function') {
      cb = opts;
      opts = null;
    }
    var self = this;

    api.remote.read(pathname + '/' + this.id, opts, function(err, db) {
      if (err) return cb(err, self);
      if (typeof db === 'string') db = JSON.parse(db);
      xtend(self, mkmodel(db));
      cb(null, self);
    });
  };

  Model.prototype.get = function(regex) {
    for (var i in this) {
      if (i.match(regex)) {
        return this[i];
      }
    }
  };

  Model.prototype.mkmedia = function(foldername) {
    if (!this[foldername]) return [];
    var media = [];
    for (var i in this[foldername]) {
      if (!i.match(/\.link/)) continue;
      var m = this[foldername][i];
      media.push('/media/' + m.id + '/file.' + m['type.txt']);
    }
    return media;
  };

  function mkmodel(db) {
    var files = {};
    for (var name in db.files) {
      var data = db.files[name].data;
      if (name.match(/\.link$/)) files[name] = mkmodel(data);
      else files[name] = data;
    }
    var model = new Model(files);
    model.id = db.id;
    for (var i in db.folders) {
      model[i] = mkmodel(db.folders[i]);
    }
    return model;
  }

  return Model;
};
