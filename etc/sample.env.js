module.exports = {

  // remote credentials
  remote_user: 'root',
  remote_host: 'madrew.smpc.io',
  remote_port: '29316',

  // remote database location
  remote_db: '/root/madrew/var',

  // local database location
  local_db: 'var',

  // server settings
  host: '::',       // listen on all interfaces IPv4+6
  port: '8000',     // listen on port 8000

};
