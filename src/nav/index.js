var hg = require('hyperglue2');
var html = require('./index.html');
var Nav = require('./model');

module.exports = NavView;

function NavView() {
  this.el = hg(html);
}

NavView.prototype.show = function() {
  var self = this;

  if (this.showing) return this.update();
  this.showing = true;

  Nav.read({ sort: 'sort.txt' }, function(err, models) {
    if (err) return console.error(err);

    var items = [];
    var routes = {};
    for (var i=0; i<models.length; i++) {
      var model = models[i];
      var html = model['media.txt'] ? '<img src="/media/' + model['media.txt'] + '">' : (model['title.html'] || model['title.txt'] || model.id);
      var href = model['href.txt'] || '/' + model.id.toLowerCase();
      if (i === 1) {
        items.push({ _attr: { class: 'spacer' }});
      }
      items.push({
        _html: html,
        _attr: {
          href: href,
          target: /^\//.test(href) ? null : 'blank',
        },
      });
    }

    hg(self.el, { 'a': items });

    self.update();
  });
};

NavView.prototype.update = function() {
  var regex = new RegExp('^/' + window.location.pathname.split('/')[1] + '(/.*)?$');
  var links = this.el.querySelectorAll('a');
  for (var i=0; i<links.length; i++) {
    var link = links[i];
    if (regex.test(link.pathname)) {
      link.className = 'active';
    }
    else if (link.className !== 'spacer') {
      link.className = '';
    }
  }
};
