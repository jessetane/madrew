var hg = require('hyperglue2');
var md = require('marked');
var html = require('./index.html');
var Page = require('./model');

var router = require('uri-router');
var FeedView = require('../feeds');
var ItemView = require('../items');

var first = true;

module.exports = PageView;

function PageView() {
  this.el = hg(html);
  if (!first) {
    this.el.className = 'page inactive out';
  }
  first = false;
  this.feedrouter = router({
    watch: 'pathname',
    outlet: this.el.querySelector('.page-feed'),
    routes: {
      '^/[^/]*(/.*)?': FeedView,
    }
  });
}

PageView.prototype.show = function(router) {
  var self = this;
  var parts = window.location.pathname.split('/');
  var id = parts[1] === '' ? 'home' : parts[1];
  var model = this.model = new Page({ id: id });

  if (this.router) return;
  this.router = router;

  var loader = document.querySelector('.loader');
  var needsloader = setTimeout(function() {
    loader.className = 'loader';
  }, 250);

  model.read(function(err, data) {
    clearTimeout(needsloader);
    loader.className = 'loader transparent';
    
    if (err) return console.error(err);

    hg(self.el, {
      '.page-status': { _attr: { class: 'page-status transparent' }},
      '.page-body': { _html: md(data['body.md'] || '') },
    });

    // force repaint
    var t = window.getComputedStyle(self.el);
    t.transform;
    t.opacity;

    // animate in
    self.el.className = 'page animate';

    // bg
    var bg = document.querySelector('.bg');
    var prev = bg.lastChild;
    if (prev) {
      prev.className = 'bg-image animate transparent';
      prev.addEventListener('transitionend', function() {
        prev.parentNode.removeChild(prev);
      });
    }
    if (data['background.txt']) {
      var next = hg('<div class="bg-image transparent"></div>', {
        _attr: { style: 'background-image:url(/media/' + data['background.txt'] + ');' }
      });
      bg.appendChild(next);
      var t = window.getComputedStyle(next);
      t.opacity;
      next.className = 'bg-image animate';
      if (data['opacity.txt']) next.style.opacity = data['opacity.txt'];
    }
  });
};

PageView.prototype.hide = function(cb) {
  this.feedrouter.destroy();
  this.el.addEventListener('transitionend', cb);
  this.el.className = 'page animate inactive out';
};
