var hg = require('hyperglue2');
var md = require('marked');
var rc = require('random-color');
var scroll = require('scroll');
var router = require('uri-router');
var html = require('./index.html');
var Page = require('../pages/model');
var mkitem = require('../../lib/model');

module.exports = ItemView;

function ItemView() {
  this.el = hg(html);
}

ItemView.prototype.show = function() {
  if (this.showing) return;

  var self = this;
  var parts = window.location.pathname.split('/');
  var pageid = parts[1] === '' ? 'home' : parts[1];
  var itemid = parts[2];

  this.el.addEventListener('click', function(evt) {
    if (/item-close/.test(evt.target.className)) {
      var parts = window.location.pathname.split('/');
      router.push(parts.pop() && parts.join('/'));
    }
  });

  var loader = document.querySelector('.loader');
  var needsloader = setTimeout(function() {
    loader.className = 'loader';
  }, 250);

  var page = new Page({ id: pageid });
  page.read(function(err, page) {
    if (err) return console.error(err);
    if (!page['feed.txt']) return console.error(new Error('feed not found', 404));

    var Item = mkitem(page['feed.txt']);
    var item = new Item({ id: itemid });
    item.read(function(err) {
      clearTimeout(needsloader);
      loader.className = 'loader transparent';

      if (err) return console.error(err);

      hg(self.el, {
        _attr: { class: 'item animate' },
        '.item-date': new Date(item['date.date']).toLocaleDateString(),
        '.item-title': { _html: item['title.txt'] || item['name.txt'] || item.id },
        '.item-body': { _html: md(item['body.md']) },
      });
    });
  });
};

ItemView.prototype.hide = function(cb) {
  var a = require('../feeds').animation;
  this.el.className = 'item inactive animate';
  scroll.top(document.body, 0, a);
  setTimeout(function() {
    cb && cb();
  }, a.duration);
};

// ItemView.prototype.update = function(active) {
//   if (this.active === active) return;
//   this.active = active;
//   this.scroller = this.el.querySelector('.item-scroller');

//   if (!this.active && this.scroller.scrollTop) {
//     scroll.top(this.scroller, 0, require('./').animation);
//   }

//   hg(this.el.parentNode, { _attr: { class: active ? 'item active' : 'item' }});

//   var self = this;
//   if (active) {
//     setTimeout(function() {
//       hg(self.el.parentNode, { _attr: { style: 'width:' + window.innerWidth + 'px;' }});
//     }, require('./').animation.duration);
//   }
//   else {
//     hg(self.el.parentNode, { _attr: { style: null }});
//   }
// };
