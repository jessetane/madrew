var hg = require('hyperglue2');
var md = require('marked');
var rc = require('random-color');
var router = require('uri-router');
var scroll = require('scroll');
var html = require('./index.html');
var Page = require('../pages/model');
var mkitem = require('../../lib/model');
var ItemView = require('../items');

module.exports = FeedView;

FeedView.last = {};
FeedView.animation = {
  duration: 350,
  ease: 'inOutSine',
};

function FeedView() {
  this.el = hg(html);
  this.itemrouter = router({
    watch: 'pathname',
    outlet: [].slice.call(document.querySelectorAll('.page-item')).slice(-1)[0],
    routes: {
      '^/[^/]*/[^/]*(/.*)?': ItemView,
    }
  });
}

FeedView.prototype.show = function(router) {
  var self = this;
  var parts = window.location.pathname.split('/');
  var pageid = parts[1] === '' ? 'home' : parts[1];
  var model = new Page({ id: pageid });

  if (this.showing) return this.update(parts[2]);

  this.showing = true;
  this.router = router;
  this.cycle = cycle.bind(this);
  this.resize = resize.bind(this);
  this.pages = document.querySelector('.pages');
  window.addEventListener('keyup', this.cycle);
  window.addEventListener('resize', this.resize);

  model.read(function(err, data) {
    if (err) return console.error(err);

    if (data['feed.txt']) {
      var Item = mkitem(data['feed.txt']);
      Item.read({ position: 0, limit: 50, sort: 'date.date' }, function(err, models) {
        if (err) return console.error(err);

        hg(self.el, {
          '.feed-item': models.map(function(model) {
            return {
              _attr: { href: '/' + pageid + '/' + model.id, 'data-id': model.id },
              '.feed-item-bg-color': { _attr: { style: 'background-color:' + (model['color.txt'] ? model['color.txt'] : (model['media.txt'] ? '#000' : '#555')) + ';' }},
              '.feed-item-bg-media': { _attr: { style: model['media.txt'] ? 'background-image:url(/media/' + model['media.txt'] + ');' : null }},
              '.feed-item-preview .date': new Date(model['date.date']).toLocaleDateString(),
              '.feed-item-preview h1': model['title.txt'] || model['name.txt'] || model.id,
              '.feed-item-preview p': { _html: md(getPreview(model)) },
            }
          })
        });

        self.resize();
        self.update(parts[2]);
      });
    }
    else {
      hg(self.el, { '.feed-item': [] });
    }
  });
};

FeedView.prototype.update = function(itemid) {
  if (itemid) {
    this.itemid = itemid;

    var feed = document.querySelector('.page-feed');
    var active = this.el.querySelector('[data-id=' + itemid + ']');
    var maxscroll = feed.firstChild.offsetWidth - window.innerWidth;
    var center = active.offsetLeft - (window.innerWidth / 2 - active.offsetWidth / 2);

    center = center > 0 ? center : 0;
    center = center < maxscroll ? center : maxscroll;

    if (!this.firstupdate) {
      feed.scrollLeft = center;
      oncenter.call(this);
    }
    else {
      scroll.left(feed, center, FeedView.animation, oncenter.bind(this));
    }
  }
  else {
    var feed = document.querySelector('.page-feed');
    feed.className = 'page-feed';

    if (this.itemid) {
      delete this.itemid;

      var active = this.el.querySelector('[data-id=' + this.itemid + ']');
      var items = [].slice.call(this.el.querySelectorAll('.feed-item'));
      for (var i in items) {
        items[i].style.webkitTransform = '';
        items[i].style.mozTransform = '';
        items[i].style.transform = '';
      }
      feed.scrollLeft = this.lastscroll;
    }
    this.el.style.pointerEvents = '';
    this.el.style.opacity = '1';
  }

  this.firstupdate = true;

  function oncenter() {
    var left = active.offsetLeft - feed.scrollLeft;
    var right = window.innerWidth - left - active.offsetWidth;
    var items = [].slice.call(this.el.querySelectorAll('.feed-item'));
    var after = false;
    for (var i in items) {
      var item = items[i]
      if (item === active) {
        after = true;
        continue;
      }
      if (after) {
        item.style.webkitTransform = 'translate3d(' + right + 'px,0,0)';
        item.style.mozTransform = 'translate3d(' + right + 'px,0,0)';
        item.style.transform = 'translate3d(' + right + 'px,0,0)';
      }
      else {
        item.style.webkitTransform = 'translate3d(-' + left + 'px,0,0)';
        item.style.mozTransform = 'translate3d(-' + left + 'px,0,0)';
        item.style.transform = 'translate3d(-' + left + 'px,0,0)';
      }
    }

    this.lastscroll = feed.scrollLeft;
    this.el.style.pointerEvents = 'none';
    this.el.style.opacity = '0';

    var self = this;
    setTimeout(function() {
      if (self.itemid) {
        feed.className = 'page-feed hidescroll';
      }
    }, FeedView.animation.duration);
  }
};

FeedView.prototype.hide = function(cb) {
  window.removeEventListener('keyup', this.cycle);
  window.removeEventListener('resize', this.resize);
  this.itemrouter.destroy();
  cb && cb();
};

function cycle(evt) {
  if (!this.itemid) return;
  var active = this.el.querySelector('[data-id=' + this.itemid + ']');
  if (evt.keyCode === 37) {
    if (active.previousSibling) {
      var id = active.previousSibling.getAttribute('data-id');
      router.push(window.location.pathname.replace(/(.*\/).*/, '$1' + id));
    }
  }
  else if (evt.keyCode === 39) {
    if (active.nextSibling) {
      var id = active.nextSibling.getAttribute('data-id');
      router.push(window.location.pathname.replace(/(.*\/).*/, '$1' + id));
    }
  }
}

function getPreview(model) {
  var preview = model['preview.txt'];
  var previewlength = 140;
  if (!preview) {
    preview = model['body.md'];
    if (model['body.md'].length > previewlength) {
      var preview = model['body.md'].slice(0, previewlength);
      var rest = model['body.md'].slice(previewlength);
      while (rest[0] && rest[0] !== ' ' && rest[0] !== '\n' && rest[0] !== ',' && rest[0] !== '.') {
        preview += rest.slice(0, 1);
        rest = rest.slice(1);
      }
      preview += ' ...';
    }
  }
  return preview;
}

function resize(evt) {
  var totalheight = window.innerHeight;
  var height = totalheight - (this.el.offsetTop + this.pages.offsetTop);
  var items = [].slice.call(this.el.querySelectorAll('.feed-item-wrapper'));
  for (var i in items) {
    var el = items[i];
    el.style.height = height + 'px';
  }
}
